﻿using HomeCare.Configurations;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace HomeCare.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender
    {
         public readonly EmailSettings _config;

        public EmailSender(IOptions<EmailSettings> option) {

            //_config = option.Value;
        }


        public Task SendEmailAsync(string email, string subject, string msg)
        {

        //    var message = new MimeMessage();

        //    message.From.Add(new MailboxAddress(_config.AdminEmail));
        //    message.To.Add(new MailboxAddress("Arthur Ezeagbo", email));
        //    message.Subject = subject;
        //    message.Cc.Clear();

        //    message.Body = new TextPart("plain")
        //    {
        //        Text = @msg
        //    };

        //    using (var client = new SmtpClient())
        //    {
        //        client.Timeout = 3000;
        //        // client.ServerCertificateValidationCallback = (s, c, h, e) => true;

        //        client.Connect(_config.Host, _config.Port, _config.EnableSsl);

        //        // client.Authenticate(_config.MyEmail, _config.Password);

        //        client.Send(message);
        //        client.Disconnect(true);

                return Task.CompletedTask;
            
        }
    }
}
