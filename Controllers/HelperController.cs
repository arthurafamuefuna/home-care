﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HomeCare.Data;
using HomeCare.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace HomeCare.Controllers
{
    [Authorize(Roles = ""+AppUtils.AppRoles.ADMIN+","+AppUtils.AppRoles.GUARANTOR+"")]
    public class HelperController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public HelperController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Helper
        public async Task<IActionResult> Index()
        {
            var userId = _userManager.GetUserId(User);
            
            if(User.IsInRole(AppUtils.AppRoles.ADMIN))   return View(await _context.Helper.ToListAsync());

            return View(await _context.Helper.Where(c => c.Guarantor.UserId.Equals(userId)).ToListAsync());
        }

        // GET: Helper/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var helperModel = await _context.Helper
                .SingleOrDefaultAsync(m => m.Id == id);
            if (helperModel == null)
            {
                return NotFound();
            }

            return View(helperModel);
        }

        // GET: Helper/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Helper/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Surname,Firstname,Lastname,Gender,Religion,Qualification,Language,Status,NextOfKinName,NextOfKinAddress,NextOfKinPhone")] HelperModel helperModel)
        {
            var userId = _userManager.GetUserId(User);

            if (ModelState.IsValid)
            {
                var staffId = _context.Guarantor.SingleOrDefault(c => c.UserId.Equals(userId)).Id;
                helperModel.GurantorId = staffId;

                _context.Add(helperModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(helperModel);
        }

        // GET: Helper/Edit/5
        [Authorize(Roles = AppUtils.AppRoles.GUARANTOR)]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var helperModel = await _context.Helper.SingleOrDefaultAsync(m => m.Id == id);
            if (helperModel == null)
            {
                return NotFound();
            }
            return View(helperModel);
        }

        // POST: Helper/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Surname,Firstname,Lastname,Gender,Religion,Qualification,Language,Status,NextOfKinName,NextOfKinAddress,NextOfKinPhone")] HelperModel helperModel)
        {
            if (id != helperModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(helperModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HelperModelExists(helperModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(helperModel);
        }

        // GET: Helper/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var helperModel = await _context.Helper
                .SingleOrDefaultAsync(m => m.Id == id);
            if (helperModel == null)
            {
                return NotFound();
            }

            return View(helperModel);
        }

        // POST: Helper/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var helperModel = await _context.Helper.SingleOrDefaultAsync(m => m.Id == id);
            _context.Helper.Remove(helperModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HelperModelExists(int id)
        {
            return _context.Helper.Any(e => e.Id == id);
        }
    }
}
