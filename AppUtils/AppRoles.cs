﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeCare.AppUtils
{
    public class AppRoles
    {
        public const string ADMIN = "Admin";
        public const string GUARANTOR = "Guarantor";
        //public const string HELPER = "Helper";
        public const string EMPLOYER = "Employer";
    }
}

