﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HomeCare.Data;
using HomeCare.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace HomeCare.Controllers
{
    public class GuarantorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public GuarantorController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Guarantor
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> ViewHelpers()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Helper.Where(c => c.Guarantor.Id.Equals(user.Id)).ToListAsync());
        }

        // GET: Guarantor/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guarantorModel = await _context.Guarantor
                .SingleOrDefaultAsync(m => m.Id == id);
            if (guarantorModel == null)
            {
                return NotFound();
            }

            return View(guarantorModel);
        }

        [Authorize]
        // GET: Guarantor/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Guarantor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,HomeAddress,OfficeAddress,LGA,State,Country,Phone,Email,Surname,Firstname,Lastname,Gender,Religion,CreatedAt,UpdatedAt")] GuarantorModel guarantorModel,IFormFile photo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(guarantorModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(guarantorModel);
        }

        // GET: Guarantor/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guarantorModel = await _context.Guarantor.SingleOrDefaultAsync(m => m.Id == id);
            if (guarantorModel == null)
            {
                return NotFound();
            }
            return View(guarantorModel);
        }

        // POST: Guarantor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,HomeAddress,OfficeAddress,LGA,State,Country,Phone,Email,Photo,Surname,Firstname,Lastname,Gender,Religion,CreatedAt,UpdatedAt")] GuarantorModel guarantorModel)
        {
            if (id != guarantorModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(guarantorModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GuarantorModelExists(guarantorModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(guarantorModel);
        }

        // GET: Guarantor/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guarantorModel = await _context.Guarantor
                .SingleOrDefaultAsync(m => m.Id == id);
            if (guarantorModel == null)
            {
                return NotFound();
            }

            return View(guarantorModel);
        }

        // POST: Guarantor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var guarantorModel = await _context.Guarantor.SingleOrDefaultAsync(m => m.Id == id);
            _context.Guarantor.Remove(guarantorModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GuarantorModelExists(int id)
        {
            return _context.Guarantor.Any(e => e.Id == id);
        }
    }
}
