﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeCare.AppUtils
{
    public enum HelperStatusEnum
    {
        Taken = 0,
        Available = 1,
        Held = 2
    }
}
