﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HomeCare.Data;
using HomeCare.Models;

namespace HomeCare.Controllers
{
    public class EmployerController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EmployerController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Employer
        public async Task<IActionResult> Index()
        {
            return RedirectToAction(nameof(HomeController.Index));
        }

        // GET: Employer/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employerModel = await _context.Employer
                .SingleOrDefaultAsync(m => m.Id == id);
            if (employerModel == null)
            {
                return NotFound();
            }

            return View(employerModel);
        }

        // GET: Employer/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Surname,Firstname,Lastname,Gender,Religion,Occupation,HomeAddress,OfficeAddress,LGA,State,Country,Phone,Email,Photo")] EmployerModel employerModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employerModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(employerModel);
        }

        // GET: Employer/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employerModel = await _context.Employer.SingleOrDefaultAsync(m => m.Id == id);
            if (employerModel == null)
            {
                return NotFound();
            }
            return View(employerModel);
        }

        // POST: Employer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Surname,Firstname,Lastname,Gender,Religion,Occupation,HomeAddress,OfficeAddress,LGA,State,Country,Phone,Email,Photo")] EmployerModel employerModel)
        {
            if (id != employerModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employerModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployerModelExists(employerModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employerModel);
        }

        // GET: Employer/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employerModel = await _context.Employer
                .SingleOrDefaultAsync(m => m.Id == id);
            if (employerModel == null)
            {
                return NotFound();
            }

            return View(employerModel);
        }

        // POST: Employer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employerModel = await _context.Employer.SingleOrDefaultAsync(m => m.Id == id);
            _context.Employer.Remove(employerModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployerModelExists(int id)
        {
            return _context.Employer.Any(e => e.Id == id);
        }
    }
}
