﻿using HomeCare.AppUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HomeCare.Models
{
    public class HelperModel : BaseModel
    {

        public HelperModel()
        {
            Status = HelperStatusEnum.Available;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Guarantor")]
        public int GurantorId { get; set; }
        public GuarantorModel Guarantor { get; set; }

        public string Qualification { get; set; }

        public string Language { get; set; }

        public HelperStatusEnum Status { get; set; }

        public string NextOfKinName { get; set; }

        public string NextOfKinAddress { get; set; }

        public string NextOfKinPhone { get; set; }

    }
}
