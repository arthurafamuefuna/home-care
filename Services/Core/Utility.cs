﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HomeCare.Services.Core
{
    public class Utility : Controller
    {
        private IHostingEnvironment _env;

        public Utility(IHostingEnvironment env)
        {
            _env = env;
        }

        //This method uploads a list of pdf files to the wwwroot\uploads
        public IActionResult UploadFile(List<IFormFile> formFile)
        {

            var root = _env.WebRootPath + @"\pdfUploads";

            if (formFile.Count > 0 && formFile.Count < 6)
            {
                foreach (var file in formFile)
                {

                    var filename = Path.Combine(@root, Path.GetFileName(file.FileName));

                    if (file == null)
                    {
                        ViewData["Result"] = "Choose a file";

                        return View("UploadPdf");
                    }

                    if (Path.GetExtension(file.FileName) != ".pdf")
                    {
                        ViewData["Result"] = "Invalid format, PDF required!";

                        return View("UploadPdf");
                    }

                    if (file.Length > 3072000)
                    {
                        ViewData["Result"] = "File size is cannot be more than 3MB";
                        return View("UploadPdf");
                    }

                    try
                    {

                        file.CopyToAsync(new FileStream(filename, FileMode.Create));
                    }
                    catch (IOException ex)
                    {
                        ViewData["Result"] = "Upload failed! Ensure you don't upload a file multiple times";

                        return View("UploadPdf");
                    }


                }

                ViewData["Result"] = "Upload successful";

                return View("UploadPdf");

            }
            else
            {
                ViewData["Result"] = "Upload Failed! Ensure that number of files do not exceed 5(five) copies";

                return View("UploadPdf");
            }

        }


        //This method uploads guarantors images to the wwwroot\imageUploads folder and returns the file location and name
        public string UploadImage(IFormFile formFile)
        {

            var root = _env.WebRootPath + @"\imageUploads";


            var filename = Path.Combine(@root, Path.GetFileName(formFile.FileName));

            if (formFile == null)
            {

                return null;
            }

            if (Path.GetExtension(formFile.FileName) != ".jpg")
            {
                ViewData["Result"] = "Invalid format, image required!";

                return null;
            }

            if (formFile.Length > 3072000)
            {
                ViewData["Result"] = "File size cannot be more than 3MB";
                return "Exceeded file size limit";
            }

            try
            {

                formFile.CopyToAsync(new FileStream(filename, FileMode.Create));
                ViewData["Result"] = "File uploaded and saved successfully";

                return filename;
            }
            catch (IOException)
            {
                ViewData["Result"] = "Upload failed! Ensure you don't upload a file multiple times";

                return "Upload failed";
            }

        }

    }
}
